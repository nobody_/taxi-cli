#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import hashlib
import requests
import json
import dateutil.parser
import datetime
import logging

class Taxi(object):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        logging.basicConfig(level = logging.WARNING, format='%(name)s: [%(asctime)s] %(message)s',)
        self.__logger     = logging.getLogger(str(self.__class__.__name__))
        self.__credsFile  = u'./.creds'
        self.__bearerFile = u'./.token'
        self.__username   = None
        self.__password   = None
        self.__bearer     = None
        self.__endPoints  = {
            'login': '/api/auth',
            'logout': '',
            'discovery': '/api/v1/addresses?q=%s&limit=10',
            'history'  : '/api/v1/history/orders/%u/%u',
            'orders'   : '/api/v1/orders/%s?fields=%s',
            'cost'     : '/api/v1/orders/cost',
            'cancel'   : '/api/v1/orders/%s/cancel',
            'new_order': '/api/v1/orders',
        }
        self.__car_types = [
            u'Standart',
            u'Premium',
            u'Business',
            u'Wagon',
            u'MiniVan',
        ]

        self.__locale        = 'ru'
        self.__city_id       = 1
        self.__phone         = u'+30991234567'
        self.__client_name   = u'Маджахед'
        ## res/values/strings.xml
        self.__client_id     = 'f42222a32583453089a77af021bca83cn'
        self.__client_secret = '78-8E-33-4F-C7-31-10-A1-9F-33-10-8D-1C-E4-B2-62'

        self.__API           = 'https://ssl.uklon.com.ua'
        self.__proxies       = {} #'https': 'http://127.0.0.1:8080'}
        self.__verifySSLPeer = True

        self.__from          = None
        self.__from_number   = None
        self.__to            = None
        self.__to_number     = None
        self.__extra_cost    = 0
        self.__car_type      = self.__car_types[0]
        self.__entrance      = 0
        self.__order         = {}
        self.__order_id      = None

        self.__headers = {
            'User-Agent': 'UklonAndroid/137',
            'locale'    : str(self.__locale),
            'city'      : str(self.__city_id),
        }

    def setVerbose(self):
        self.__logger.setLevel(logging.DEBUG)

    def setFrom(self, address, number):
        self.__from = address
        self.__from_number = number

    def setTo(self, address, number):
        self.__to = address
        self.__to_number = number

    def setExtraCost(self, extra):
        self.__extra_cost = int(extra)

    def getExtraCost(self):
        return self.__extra_cost

    def getOrder(self):
        return self.__order

    def setCarType(self, car_type):
        if car_type not in self.__car_types:
            raise
        self.__car_type = car_type

    def getCarType(self):
        return self.__car_type

    def getCarTypes(self):
        return self.__car_types

    def getOrderId(self):
        return self.__order_id

    def __loadCreds(self):
        ## ./ua/com/uklon/internal/btw.class
        self.__logger.debug('[!] Loading creds from %s', self.__credsFile)

        with open(self.__credsFile, 'r') as fd:
            self.__username, self.__password = fd.read().splitlines()
            self.__passwordHash = hashlib.md5(self.__password.encode())
            self.__passwordHash = self.__passwordHash.hexdigest().upper()
            self.__passwordHash = '-'.join([a + b for a, b in zip(*[iter(self.__passwordHash)]*2)])

    def __loginRequestLazy(self):
        self.__logger.debug('[!] Loading token from %s', self.__bearerFile)
        bearer = None
        try:
            with open(self.__bearerFile, 'rb') as fd:
                bearer = json.load(fd)
        except FileNotFoundError:
            self.__logger.debug('[-] %s not found', self.__bearerFile)
            return

        expire_date = bearer['expires']
        timeout     = int(bearer['expires_in'])

        expires_in  = dateutil.parser.parse(expire_date)
        expires_in += datetime.timedelta(seconds = timeout)
        now         = datetime.datetime.now(expires_in.tzinfo)

        if now > expires_in:
            self.__logger.debug('[-] Saved token expired')
            return

        return bearer

    def __loginRequestReal(self):
        self.__loadCreds()

        self.__logger.debug('[!] OAuth request')
        data = {
            'username'     : self.__username,
            'password'     : self.__passwordHash,
            'grant_type'   : u'password',
            'client_id'    : self.__client_id,
            'client_secret': self.__client_secret,
        }

        api = '%s/%s' % (self.__API, self.__endPoints['login'])
        req = requests.post(api, data = data, headers = self.__headers, proxies = self.__proxies, verify = self.__verifySSLPeer)

        if not req.ok:
            self.__logger.debug('[!] Auth failed')
            sys.exit(1)
            return

        self.__logger.debug('[+] Storing bearer token to %s', self.__bearerFile)

        with open(self.__bearerFile, 'wb') as fd:
            fd.write(req.content)
        return req.json()

    def loginRequest(self, force = False):
        self.__bearer = self.__loginRequestLazy()
        if not self.__bearer or force:
            self.__bearer = self.__loginRequestReal()

        self.__headers.update({
            'Authorization': 'Bearer %s' % (self.__bearer['access_token'])
        })

    def order_info(self, order_id = None, fields = ['cost']):
        if not order_id:
            return
        self.__logger.debug('[!] Requesting order information')
        api = '%s/%s' % (self.__API, self.__endPoints['orders'])
        api = api % (order_id, ','.join(fields))

        req = requests.get(api, headers = self.__headers, proxies = self.__proxies, verify = self.__verifySSLPeer)
        if req.status_code == 401:
            self.loginRequest(force = True)
            self.order_info(order_id, fields)
            return

        if not req.ok:
           return

        if not req.content:
            return

        req.json()
        return req.json()

    #def history(self, offset = 1, count = 10):
    #    api = '%s/%s' % (self.__API, self.__endPoints['history'])
    #    api = api % (offset, count)
    #    self.__logger.debug('history <-> [%u/%u]', offset, count)

    #    req = requests.get(api, headers = self.__headers, proxies = self.__proxies, verify = self.__verifySSLPeer)
    #    if req.status_code == 401:
    #        self.loginRequest(force = True)
    #        self.history(offset, count)
    #        return

    #    if not req.ok:
    #        raise

    #    if not req.content:
    #        return

    #    hist = req.json()
    #    self.orders.extend( [order['uid'] for order in hist['items']] )
    #    return hist['has_more_items']

    def discovery(self, street = None):
        if not street:
            raise ValueError

        self.__logger.debug('[!] Discovering "%s"', street)
        api = '%s/%s' % (self.__API, self.__endPoints['discovery'])
        api = api % (street)

        req = requests.get(api, headers = self.__headers, proxies = self.__proxies, verify = self.__verifySSLPeer)
        if req.status_code == 401:
            self.loginRequest(force = True)
            self.discovery(street)
            return

        if not req.ok:
            raise

        if not req.content:
            return

        return req.json()

    def newOrder(self):
        self.__logger.debug('[+] Making new order')
        api = '%s/%s' % (self.__API, self.__endPoints['new_order'])

        order = self.__prepare_order()

        req = requests.post(api, json = order, headers = self.__headers, proxies = self.__proxies, verify = self.__verifySSLPeer)
        if req.status_code == 401:
            self.loginRequest(force = True)
            self.newOrder()
            return
        json = req.json()

        if req.status_code != 201:
            if 'error' in json:
                return (json['error']['message'], False)
            return

        if not req.ok:
            return

        if 'uid' not in req.json():
            self.__logger.critical('Order processed but no UID has been assigned!\n\n%s', req.content)
            sys.exit(1)
        self.__order_id = req.json()['uid']
        return self.__order_id

    def cancelOrder(self, order_id):
        self.__logger.debug('[!] Canceling order [%s]', order_id)
        if not order_id:
            order_id = self.__order_id

        api = '%s/%s' %( self.__API, self.__endPoints['cancel'])
        api = api % order_id

        data = {
            "cancel_comment": "",
            "client_cancel_reason":"timeout",
        }

        req = requests.put(api, json = data, headers = self.__headers, proxies = self.__proxies, verify = self.__verifySSLPeer)
        if req.status_code == 401:
            self.loginRequest(force = True)
            self.cancelOrder()
            return

        return req.ok

    def cost(self):
        self.__logger.debug('[!] Calculating cost')
        api = '%s/%s' % (self.__API, self.__endPoints['cost'])

        order = self.__prepare_order()

        req = requests.post(api, json = order, headers = self.__headers, proxies = self.__proxies, verify = self.__verifySSLPeer)

        if req.status_code == 401:
            self.loginRequest(force = True)
            self.cost()
            return

        if not req.ok:
            return

        return req.json()

    def __prepare_order(self):
        self.__order = {
          "add_conditions": [],
          "car_type": self.__car_type,
          "route": {
            "route_points": [
              {
                "address_name": self.__from,
                "house_number": self.__from_number,
                "city_id": self.__city_id,
                "is_place": False,
                "lat": 0,
                "lng": 0
              },
              {
                "address_name": self.__to,
                "house_number": self.__to_number,
                "city_id": self.__city_id,
                "is_place": False,
                "lat": 0,
                "lng": 0
              }
            ],
            "comment": "",
            "entrance": self.__entrance,
            "is_office_building": False
          },
          "client_name": self.__client_name,
          "payment_type": u'cash',
          "phone": self.__phone,
          "extra_cost": self.__extra_cost,
          "city_id": self.__city_id,
          "uklon_driver_only": False
        }
        return self.__order

