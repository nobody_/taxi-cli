#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import argparse
import time
import math
import signal
from uklon import Taxi

uklon = None

def sig_handler(signal, frame):
    global uklon
    uid = uklon.getOrderId()
    if uid:
        uklon.cancelOrder(uid)
    sys.exit(1)

def display_order_info(order):
    white_star    = u'\u2606'
    black_star    = u'\u2605'
    pinwheel_star = u'\u272F'
    max_stars     = 5

    pickup_at  = int(order['pickup_time'])
    print('-' * 20)
    print('Arrival: %s' % (time.strftime('%H:%M', time.gmtime(pickup_at))))

    if 'driver' in order:
        rating = order['driver']['rating']
        full_stars  = math.floor(rating) % max_stars
        empty_stars = math.ceil(rating) % max_stars
        half_stars  = (max_stars - abs( full_stars - empty_stars )) % max_stars
        s_rating    = u''
        s_rating   += black_star * full_stars
        s_rating   += pinwheel_star * half_stars
        s_rating   += white_star * empty_stars
        print('Rating : %s' % s_rating)
    if 'vehicle' in order:
        license = order['vehicle']['license_plate']
        brand = order['vehicle']['make']
        model = order['vehicle']['model']
        color = order['vehicle']['color']
        comfort = order['vehicle']['comfort_level'].capitalize()
        print('Car    : %s' % (brand))
        print('Numbers: %s' % (license))
        if model:
            print('Model  : %s' % (model))
        if color:
            print('Color  : %s' % (color))
        if comfort:
            print('Type   : %s' % (comfort))

def display_address_list(addresses):
    if not addresses:
        return
    for address in addresses:
        print('%s' % address['address_name'])

def main():

    global uklon
    uklon = Taxi()

    parser = argparse.ArgumentParser(description='Taxi CLI')
    parser.add_argument('--search',        metavar='address',   type=str, help = 'Search address')
    parser.add_argument('--type',          metavar='type',      type=str, help = 'Car type', choices = uklon.getCarTypes())
    parser.add_argument('--cancel',        metavar='uid',       type=str, help = 'Cancel order')
    parser.add_argument('--src',           metavar='address',   type=str, help = 'Source address')
    parser.add_argument('--src-number',    metavar='number',    type=str, help = 'Source address number', default = '0')
    parser.add_argument('--dst',           metavar='address',   type=str, help = 'Destination address')
    parser.add_argument('--dst-number',    metavar='number',    type=str, help = 'Destination address number', default = '0')
    parser.add_argument('--extra',         metavar='amount',    type=int, help = 'Extra cost', default = 0)

    parser.add_argument('--order',         action='store_true', default = False, help = 'Make order')
    parser.add_argument('--costs',         action='store_true', default = False, help = 'Cost estimation')
    parser.add_argument('-v', '--verbose', action='store_true', default = False)

    args         = parser.parse_args()

    if args.verbose:
        uklon.setVerbose()

    uklon.loginRequest()

    if args.type:
        uklon.setCarType(args.type)
    if args.search:
        streets = uklon.discovery(street = str(args.search))
        display_address_list(streets)
        return 0
    if args.cancel:
        canceled = uklon.cancelOrder(args.cancel)
        if canceled:
            print('Canceled')
        else:
            print('Error while cancel!')
        return 0
    if args.src and args.dst:
        uklon.setFrom(args.src, args.src_number)
        uklon.setTo(args.dst, args.dst_number)
    if args.extra:
        uklon.setExtraCost(args.extra)
    if args.costs:
        cost = uklon.cost()
        extra = uklon.getExtraCost()
        if not cost:
            print('Wrong address')
            return
        print('Price: %u %s\nDistance: %.2f km' % (cost['cost'], cost['currency'], cost['distance']))
    if args.order:
        uid = uklon.newOrder()
        print('Making new order [%s]' % uid)
        progress = 0
        while True:
            if progress % 4 == 0:
                c = '-'
            elif progress % 4 == 1:
                c = '\\'
            elif progress % 4 == 2:
                c = '|'
            else:
                c = '/'
            sys.stdout.write('%c\r' % (c))
            progress += 1

            if progress % 10 == 0:
                info = uklon.order_info(order_id = uid, fields=['driver', 'cost', 'vehicle'])
                if not info:
                    continue
                if 'driver' in info:
                    display_order_info(info)
                    return 0
            time.sleep(0.3)

if __name__ == '__main__':
    signal.signal(signal.SIGINT, sig_handler)
    main()
